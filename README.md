
![image](images/EBS.png)

---
# Error Pages

> This repository contains all the necessary resources to deploy generator for HTTP error pages

## Usage
### Custom error pages for Traefik

```yaml
version: '3.4'

  error-pages:
    image: ebsproject/error-page-service:dev
    environment:
      TEMPLATE_NAME: ebs
    networks:
      - ebs_network
    ports:
      - 8082:8080
    deploy:
      labels:
        - "traefik.enable='true'"
        - traefik.docker.network=ebs_network"
        - "traefik.http.routers.error-pages-router.rule=HostRegexp(`{host:.+}`)"
        - "traefik.http.routers.error-pages-router.priority=10"
        - "traefik.http.routers.error-pages-router.tls='true'"
        - "traefik.http.routers.error-pages-router.entrypoints=https"
        - "traefik.http.routers.error-pages-router.middlewares=error-pages-middleware@docker"
        - "traefik.http.services.error-pages-service.loadbalancer.server.port=8080"
        - "traefik.http.middlewares.error-pages-middleware.errors.status=400-599"
        - "traefik.http.middlewares.error-pages-middleware.errors.service=error-pages-service@docker"
        - "traefik.http.middlewares.error-pages-middleware.errors.query=/{status}.html"

  any-another-http-service:
    image: nginx:alpine
    networks:
      - ebs_network
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.http.middlewares.any-another-http-service-https.redirectscheme.scheme=https"
        - "traefik.http.routers.any-another-http-service-http.entrypoints=web"
        - "traefik.http.routers.any-another-http-service-http.rule=Host(`localhost`)"
        - "traefik.http.routers.any-another-http-service-http.middlewares=any-another-http-service-https@docker"
        - "traefik.http.routers.any-another-http-service.tls=true"
        - "traefik.http.routers.any-another-http-service.rule=Host(`localhost`)"
        - "traefik.http.routers.any-another-http-service.entrypoints=web-secure"
        - "traefik.http.services.any-another-http-service.loadbalancer.server.port=80"
        - "traefik.http.routers.any-another-http-service.middlewares=error-pages-middleware@docker"


networks:
  ebs_network:
    driver: overlay
    name: ebs_network
```

### Customization

[1] Add html page in `templates`

[2] Update `config.json`

[3] Build the Docker image 

```bash
docker build -t ebsproject/error-page-service:dev . && docker push ebsproject/error-page-service:dev

```

## Credits and references

- [tarampampam/error-pages](https://github.com/tarampampam/error-pages)
- [template](https://codepen.io/saransh/pen/aezht)